var express = require('express');
var app = express();
const path = require('path');
const bodyParser = require('body-parser');

app.use(express.static(path.join(__dirname, 'dist')));
app.get('/', function (req, res) {
	//console.log("veo",path.join(__dirname, 'my-app/dist/index.html'))
    res.sendFile(path.join(__dirname, 'dist/index.html'));
  //res.send('Hello World!');
});

/*app.get('*', (req, res) => {
  res.sendFile(path.join(__dirname, 'transgoobal/dist/index.html'));
});*/

app.listen(3000, function () {
  console.log('Example app listening on port 3000!');
});
